# Macropad_altium_stm32f103

## Synopsis

4x4 matrix keypad with monochrome LED key lighting.


## Software
- This is modified QMK software: https://qmk.fm/ 
- Based on generic bluepill keyboard module
- I've stripped out most of the non-STM32F103 related files to ease searches, etc.
- Windows using MingGW 64-bit as outlined in the qmk instructions (its probably necessary to run the full install script first on the official build, then can compile this): https://beta.docs.qmk.fm/detailed-guides/getting_started_build_tools

- Build with command "make handwired/bluepill:default" from the "qmk_firmware" folder
- Build and flash with command "make handwired/bluepill:default:flash"

## Bootloader
- Enter bootloader by: press left encoder, hold down, press right encoder.
- Bootloader is from stm32duino here (generic_boot20_pb9.bin): https://github.com/rogerclarkmelbourne/STM32duino-bootloader/tree/master/bootloader_only_binaries 
- Flash this first with st-link or similar, install DFU drivers if needed, then dfu-util can be used to load firmware over the USB cable.
- MinGW gives an "error sending completion packet" but seems to flash OK.
- Standalone DFU command: dfu-util.exe -S "LLM 003" -D "handwired_bluepill_bluepill70_default.bin" -a 2 -R
- If you don't want to use bootloader and only flash, change the MCU_LDSCRIPT to "no bootloader" type in bluepill70/rules.mk to set memory start address 0x0800000



## License

MIT license where applicable.  