#pragma once

#include "quantum.h"
#include "led.h"


#define LAYOUT_ortho_4x4( \
  K00, K01, K02, K03,	\
  K04, K05, K06, K07,	\
  K08, K09, K10, K11,	\
  K12, K13, K14, K15   \
) { \
  { K00, K01, K02, K03 }, \
  { K04, K05, K06, K07 }, \
  { K08, K09, K10, K11 }, \
  { K12, K13, K14, K15 } \
}


#define LAYOUT_ortho_4x6( 		\
  K00, K01, K02, K03, K04, K05,	\
  K06, K07, K08, K09, K10, K11,	\
  K12, K13, K14, K15, K16, K17,	\
  K18, K19, K20, K21, K22, K23	\
) { \
  { K00, K01, K02, K03, K04, K05 }, \
  { K06, K07, K08, K09, K10, K11 }, \
  { K12, K13, K14, K15, K16, K17 }, \
  { K18, K19, K20, K21, K22, K23 } \
}

