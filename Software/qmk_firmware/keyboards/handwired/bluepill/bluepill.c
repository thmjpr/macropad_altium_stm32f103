#include "bluepill.h"

void matrix_init_kb(void) {	
  // put your keyboard start-up code here
  // runs once when the firmware starts up

  matrix_init_user();
}

void matrix_scan_kb(void) {
  // put your looping keyboard code here
  // runs every cycle (a lot)

  matrix_scan_user();
}

bool process_record_kb(uint16_t keycode, keyrecord_t *record) {
  // put your per-action keyboard code here
  // runs for every action, just before processing by the firmware

  return process_record_user(keycode, record);
}

#ifdef RGB_MATRIX_ENABLE
led_config_t g_led_config = { {
  // Key Matrix to LED Index
  {	0,	1,	2,	3 },
  {	4,	5,	6,	7 },
  {	8,	9,	10,	11 },
  {	12,	13,	14,	15 }
}, {
  // LED Index to Physical Position
  { 0,  0 }, { 1,  0 }, { 2,  0 }, { 3,  0 }, \
  { 0,  1 }, { 1,  1 }, { 2,  1 }, { 3,  1 }, \
  { 0,  2 }, { 1,  2 }, { 2,  2 }, { 3,  2 }, \
  { 0,  3 }, { 1,  3 }, { 2,  3 }, { 3,  3 }  
}, {
	/*
	Possible flags:
	#define LED_FLAG_NONE      0x00 If this LED has no flags.
	#define LED_FLAG_ALL       0xFF If this LED has all flags.
	#define LED_FLAG_MODIFIER  0x01 If the Key for this LED is a modifier.
	#define LED_FLAG_UNDERGLOW 0x02 If the LED is for underglow.
	#define LED_FLAG_KEYLIGHT  0x04 If the LED is for key backlight.
	*/
  // LED Index to Flag 
  
  LED_FLAG_KEYLIGHT, LED_FLAG_KEYLIGHT, LED_FLAG_KEYLIGHT, LED_FLAG_KEYLIGHT, \
  LED_FLAG_KEYLIGHT, LED_FLAG_KEYLIGHT, LED_FLAG_KEYLIGHT, LED_FLAG_KEYLIGHT, \
  LED_FLAG_KEYLIGHT, LED_FLAG_KEYLIGHT, LED_FLAG_KEYLIGHT, LED_FLAG_KEYLIGHT, \
  LED_FLAG_KEYLIGHT, LED_FLAG_KEYLIGHT, LED_FLAG_KEYLIGHT, LED_FLAG_KEYLIGHT
//  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
} };
#endif