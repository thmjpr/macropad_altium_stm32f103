/*
    ChibiOS - Copyright (C) 2006..2015 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#ifndef _BOARD_H_
#define _BOARD_H_
//#include <hal_usb_lld.h>


/*
 * Setup for a Generic STM32F103 board.
 */

/*
 * Board identifier.
 */
#define BOARD_GENERIC_STM32_F103
#define BOARD_NAME              "Generic STM32F103x board"

/*
 * Board frequencies.
 */
//#define STM32_LSECLK            32768
#define STM32_HSECLK            8000000

#define STM32_LSE_ENABLED 		FALSE

/*
 * MCU type, supported types are defined in ./os/hal/platforms/hal_lld.h.
 */
#define STM32F103xB
//#define STM32F103x8

/*
 * IO pins assignments
 */

/* on-board */

#define PORT_ENC				GPIOB
#define ENC0_A					0//PB0
#define ENC0_B					1//PB1
#define ENC0_SW					2//PB2

#define ENC1_A					3//PB3
#define ENC1_B					4//PB4
#define ENC1_SW					5//PB5

#define ENC2_A					6//PB6
#define ENC2_B					7//PB7


//LED stuff

#define PORT_LED				GPIOB
#define PIN_LED_B				8//PB8
#define PIN_LED_R				9//PB9
#define PIN_LED_G				10//PB10

#define LED_LE					12//PB12
#define LED_DCLK				13//PB13
#define LED_GCLK				14//PB14
#define LED_SDI					15//PB15


#define GPIOD_OSC_IN            0
#define GPIOD_OSC_OUT           1

//Slide switch
#define PORT_SLIDE		GPIOC
#define PIN_SLIDE		14



//Matrix pins
#define COL_PORT	GPIOA
#define ROW_PORT	GPIOA

#define COL_0		4//PA4
#define COL_1		5//PA5
#define COL_2		6//PA6
#define COL_3		7//PA7
#define COL_4		8//PA8
#define COL_5		9//PA9

#define ROW_0		0//PA0
#define ROW_1		1//PA1
#define ROW_2		2//PA2
#define ROW_3		3//PA3



/* Backlighting */

/*#define GPIOC_BACKLIGHT_PIN               15*/

/* In case your board has a "USB enable" hardware
   controlled by a pin, define it here. (It could be just
   a 1.5k resistor connected to D+ line.)
*/
/*
#define GPIOB_USB_DISC          10
*/

/*
 * I/O ports initial setup, this configuration is established soon after reset
 * in the initialization code.
 *
 * The digits have the following meaning:
 *   0 - Analog input.
 *   1 - Push Pull output 10MHz.
 *   2 - Push Pull output 2MHz.
 *   3 - Push Pull output 50MHz.
 *   4 - Digital input.
 *   5 - Open Drain output 10MHz.
 *   6 - Open Drain output 2MHz.
 *   7 - Open Drain output 50MHz.
 *   8 - Digital input with PullUp or PullDown resistor depending on ODR.
 *   9 - Alternate Push Pull output 10MHz.
 *   A - Alternate Push Pull output 2MHz.
 *   B - Alternate Push Pull output 50MHz.
 *   C - Reserved.
 *   D - Alternate Open Drain output 10MHz.
 *   E - Alternate Open Drain output 2MHz.
 *   F - Alternate Open Drain output 50MHz.
 * Please refer to the STM32 Reference Manual for details.
 */

/*
 * Port A setup.
 * Everything input with pull-up except:
 * PA2  - Alternate output  (USART2 TX).
 * PA3  - Normal input      (USART2 RX).
 * PA9  - Alternate output  (USART1 TX).
 * PA10 - Normal input      (USART1 RX).
 */
#define VAL_GPIOACRL            0x88888888      /*  PA7...PA0 */
#define VAL_GPIOACRH            0x88888888      /* PA15...PA8 */
#define VAL_GPIOAODR            0xFFFFFFFF

/*
 * Port B setup.
 * Everything input with pull-up except:
 */
#define VAL_GPIOBCRL            0x88888888      /*  PB7...PB0 */
#define VAL_GPIOBCRH            0x88888888      /* PB15...PB8 */
#define VAL_GPIOBODR            0xFFFFFFFF

/*
 * Port C setup.
 * Everything input with pull-up except:
 */
#define VAL_GPIOCCRL            0x88888888      /*  PC7...PC0 */
#define VAL_GPIOCCRH            0x88888888      /* PC15...PC8 */
#define VAL_GPIOCODR            0xFFFFFFFF

/*
 * Port D setup.
 * Everything input with pull-up except:
 * PD0  - Normal input (XTAL).
 * PD1  - Normal input (XTAL).
 */
#define VAL_GPIODCRL            0x88888844      /*  PD7...PD0 */
#define VAL_GPIODCRH            0x88888888      /* PD15...PD8 */
#define VAL_GPIODODR            0xFFFFFFFF

/*
 * Port E setup.
 * Everything input with pull-up except:
 */
 /*
#define VAL_GPIOECRL            0x88888888      //  PE7...PE0
#define VAL_GPIOECRH            0x88888888      //PE15...PE8
#define VAL_GPIOEODR            0xFFFFFFFF
*/

/*
 * USB bus activation macro, required by the USB driver.
 */
/* The point is that most of the generic STM32F103* boards
   have a 1.5k resistor connected on one end to the D+ line
   and on the other end to some pin. Or even a slightly more
   complicated "USB enable" circuit, controlled by a pin.
   That should go here.

   However on some boards (e.g. one that I have), there's no
   such hardware. In which case it's better to not do anything.
*/

#define USB_GPIO_PORT	GPIOA
#define USBDP_PIN		12
#define USBDM_PIN		11
/*
#define usb_lld_disconnect_bus(usbp) palClearPad(USB_GPIO_PORT, USBDM_PIN); palClearPad(USB_GPIO_PORT, USBDP_PIN);  palSetPadMode(USB_GPIO_PORT, USBDM_PIN, PAL_MODE_OUTPUT_PUSHPULL); palSetPadMode(USB_GPIO_PORT, USBDP_PIN, PAL_MODE_OUTPUT_PUSHPULL);

#define usb_lld_connect_bus(usbp) palClearPad(USB_GPIO_PORT, USBDM_PIN); palClearPad(USB_GPIO_PORT, USBDP_PIN); palSetPadMode(USB_GPIO_PORT, USBDM_PIN, PAL_MODE_INPUT); palSetPadMode(USB_GPIO_PORT, USBDP_PIN, PAL_MODE_INPUT);
*/

#define usb_lld_connect_bus(usbp) palSetPadMode(GPIOA, USBDP_PIN, PAL_MODE_INPUT);		//USB D+

/*
 * USB bus de-activation macro, required by the USB driver.
 */
#define usb_lld_disconnect_bus(usbp) (palSetPadMode(GPIOA, USBDP_PIN, PAL_MODE_OUTPUT_PUSHPULL)); palClearPad(GPIOA, USBDP_PIN)


#if !defined(_FROM_ASM_)
#ifdef __cplusplus
extern "C" {
#endif
  void boardInit(void);
#ifdef __cplusplus
}
#endif
#endif /* _FROM_ASM_ */

#endif /* _BOARD_H_ */
