/*
Copyright 2012,2013 Jun Wako <wakojun@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include QMK_KEYBOARD_H

#include "hal.h"
#include "ch.h"
#include "board.h"
#include "mbi5043_LED.h"

// Define layer names
#define _NORMAL 0
#define _ALTIUM 1
#define _FNTWO 	2
#define _OFF 	0xFF


//=====================================
//Macro keys enums
enum custom_keycodes {
  QMK_2OR3 = SAFE_RANGE,		//tap 2 or 3
  QMK_SHELF,					//shelf or unshelve
  QMK_MACRO
};

//=====================================
//PWM channel configuration (GClk)
static PWMConfig pwmcfg = {
   72000000,	//Frequency 72MHz
   5,			//Period -> GClk = 14MHz (max 33MHz) = ~220Hz pwm flicker
   NULL,		//Callback
   {
      {PWM_OUTPUT_DISABLED, NULL},		//CH1 [0]
	  {PWM_COMPLEMENTARY_OUTPUT_ACTIVE_HIGH, NULL},	//CH2N //CH2 = PWM_OUTPUT_ACTIVE_HIGH
	  {PWM_OUTPUT_DISABLED, NULL},		//CH3
	  {PWM_OUTPUT_DISABLED, NULL}		//CH4
   },
   0,
   0
};

//======================================
void led_gclk_set(bool state);
void reset_cpu(void);



//------------------------------------------
//Encoders

void encoder_update_user(uint8_t index, bool clockwise) {
  if (index == 0) { /* First encoder */
    if (clockwise)
	{
      tap_code(KC_VOLU);		//volume up
    } 
	else 
	{
      tap_code(KC_VOLD);		//volume down
    }
  } 
  else if (index == 1) { /* Second encoder */  
    if (clockwise) 
	{
      tap_code(KC_DOWN);		//Down key
    } 
	else 
	{
      tap_code(KC_UP);
    }
  }
  else if(index == 2) {		//Third encoder
	  if(clockwise)
	  {
		  tap_code(KC_A);
	  }
	  else
	  {
		  tap_code(KC_B);
	  }
  }
  else
  {}//error
}

//---------------------------------------------------------
//Encoder switches (used with dip switch function)
void dip_switch_update_user(uint8_t index, bool active) { 
    
	uprintf("sw index %u %u", index, active);
	
	switch (index) {		
        case 0:				//ENC0_SW (push encoder)
            if(active) 
			{ 
				tap_code(KC_MEDIA_PLAY_PAUSE);		//just the one code, nothing on release of button
				//dprintf("enc0_press ");
			} 
			else 
			{
				//dprintf("enc0_release ");
			}
            break;
        case 1:				//ENC1_SW
            if(active) 
			{ 
				if(!palReadPad(PORT_ENC, ENC0_SW))		//If enc1 pressed while ENC0 active, reset cpu
				{
					reset_cpu();
				}
		
				//dprintf("enc1_press ");
				//clicky_on(); 		//Could use for layer switching? increment to next layer, if possible.
				tap_code(KC_MUTE);
			} 
			else
			{
				//dprintf("enc1_release ");
			}
            break;
			
		case 2:				//Slide switch
			if(active)		//slid to right++
			{
				layer_on(_ALTIUM);
			}
			else
			{
				layer_off(_ALTIUM);
				layer_on(_NORMAL);		//redundant maybe
			}
			break;
        default:
			break;
    }
}


//Backlight keys:
/*
BL_TOGG Turn the backlight on or off
BL_STEP Cycle through backlight levels
BL_ON Set the backlight to max brightness
BL_OFF Turn the backlight off
BL_INC Increase the backlight level
BL_DEC Decrease the backlight level
BL_BRTG Toggle backlight breathing

Audio:
KC_MUTE
KC_VOLU

*/

//---------------------------------------

const uint16_t keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [_NORMAL] = LAYOUT_ortho_4x6(
  KC_7, 	KC_8,		KC_9,		KC_PPLS,	RGB_TOG,	RGB_TOG,	\
  KC_4,		KC_5,		KC_6,		KC_PMNS,	RGB_MOD,	RGB_MOD,	\
  KC_1,		KC_2,		KC_3,		KC_PAST,	KC_0,		KC_0,		\
  KC_0,		KC_PSLS,	KC_PDOT, 	KC_PENT,	BL_OFF,		BL_OFF),
  
  
  //Altium layer
  [_ALTIUM] = LAYOUT_ortho_4x6(
  KC_PAST, 		LSFT(KC_PAST),		LSFT(KC_S),		QMK_2OR3,		KC_0,	KC_0,	\
  LCTL(KC_W),	KC_TAB,				KC_Q,			KC_BSPACE,		KC_0,	KC_0,	\
  KC_0,			KC_0,				KC_0,			KC_0,			KC_0,	KC_0,	\
  KC_SPACE,		KC_0,				KC_0, 			KC_0,			KC_0,	KC_1),
    
  [_FNTWO] = LAYOUT_ortho_4x6(
  KC_7, 	KC_8,		KC_9,		KC_PPLS,	KC_0,	KC_0,	\
  KC_4,		KC_5,		KC_6,		KC_PMNS,	KC_0,	KC_0,	\
  KC_1,		KC_2,		KC_3,		KC_PAST,	KC_0,	KC_0,	\
  KC_0,		KC_PSLS,	KC_PDOT, 	KC_PENT,	KC_0,	KC_0),
  
  
};

//LIGHTING=====================================================================
/* Layer based ilumination, just binary */
uint32_t layer_state_set_user(uint32_t state)
{
  palClearPad(PORT_LED, PIN_LED_R);
  palClearPad(PORT_LED, PIN_LED_G);
  palClearPad(PORT_LED, PIN_LED_B);
  
  switch (biton32(state)) 
  {
  case _FNTWO:
	palSetPad(PORT_LED, PIN_LED_R);
    break;
  case _ALTIUM:
	palSetPad(PORT_LED, PIN_LED_B);
    break;
  case _NORMAL:
	palSetPad(PORT_LED, PIN_LED_G);
    break;
  case _OFF:		//LEDs off
  default:
    break;
  }	
  
  return state;
}


//---------------------------------------------------------
//When sleeping, power off LEDs

void suspend_power_down_user(void) 
{
    //rgb_matrix_set_suspend_state(true);
	
	//Turn off all LEDs 
	layer_state_set_user(_OFF);
	
	led_clear();
	led_flush();
	//led_gclk_set(FALSE);		//GClk disable, not working cant restart it?
}

void suspend_wakeup_init_user(void) 
{
    //rgb_matrix_set_suspend_state(false);
	
	//check state and set LED
	layer_state_set_user(layer_state);
	led_gclk_set(TRUE);			//GClk enable
}


void led_gclk_set(bool state)
{
	if(state)					//GClk enable
	{
	//Set up clock output on PB14 (GClk) = TIM1_CH2N = Alternate function
	palSetPadMode(PORT_LED, LED_GCLK, PAL_MODE_STM32_ALTERNATE_PUSHPULL);// PAL_MODE_ALTERNATE(2));
	pwmStart(&PWMD1, &pwmcfg);
  
	//Tim1 channel 2 (0, 1, 2, 3 in pwmcfg)
	pwmEnableChannel(&PWMD1, 1, PWM_PERCENTAGE_TO_WIDTH(&PWMD1, 5000)); 	
	}
	
	else
	{
	pwmStop(&PWMD1);			//GClk disable
	}
	
}

//======================================
// Reset processor
void reset_cpu(void)
{
	//usbStop(usbp);
	wait_ms(10);
	usb_lld_disconnect_bus();
	wait_ms(50);
	NVIC_SystemReset();
}


/*
void rgb_matrix_indicators_user(void) {
    if (userspace_config.rgb_layer_change &&
#    ifdef RGB_DISABLE_WHEN_USB_SUSPENDED
        !g_suspend_state &&
#    endif
#    if defined(RGBLIGHT_ENABLE)
        (!rgblight_config.enable && rgb_matrix_config.enable)
#    else
        rgb_matrix_config.enable
#    endif
    ) {
        switch (biton32(layer_state)) {
            case _NORMAL:
                //rgb_matrix_set_color(25, 0x7A, 0x00, 0xFF);                                          // 3
                break;
            case _ALTIUM:
                //rgb_matrix_layer_helper(HSV_RED, 1, rgb_matrix_config.speed * 8, LED_FLAG_MODIFIER);
                break;
            default: 
                //rgb_matrix_layer_helper(HSV_CYAN, mods_enabled, rgb_matrix_config.speed, LED_FLAG_MODIFIER);
                break;
            }
        }
    }
*/

//======================================
//Setup timer

/*
// GPT1 callback.
// clock for LED driver
static void gpt1cb(GPTDriver *gptp) {
  static uint8_t gclk = 0;
  if(gclk)
	  palSetPad(PORT_LED, LED_GCLK);
  else
	  palClearPad(PORT_LED, LED_GCLK);
  gclk = !gclk;
}

static const GPTConfig gpt1cfg = {
   72000000, //72000000, // x MHz timer clock.
   gpt1cb // Timer callback.
};
*/


//keyboard_post_init_* - Happens at the end of the firmware's startup process. This is where you'd want to put "customization" code, for the most part.

void keyboard_post_init_user(void) {
  // Customise these values to desired behaviour
  debug_enable=true;
  debug_matrix=true;
  //debug_keyboard=true;

  //Set up pins for LED driver
  palSetPadMode(PORT_LED, LED_DCLK, PAL_MODE_OUTPUT_PUSHPULL);		//Data clock
  palClearPad(PORT_LED, LED_DCLK);
  
  palSetPadMode(PORT_LED, LED_SDI, PAL_MODE_OUTPUT_PUSHPULL);		//Serial data input
  palClearPad(PORT_LED, LED_SDI);
  
  palSetPadMode(PORT_LED, LED_LE, PAL_MODE_OUTPUT_PUSHPULL);		//Latch enable
  palClearPad(PORT_LED, LED_LE);
  
  //palSetPadMode(PORT_LED, LED_GCLK, PAL_MODE_OUTPUT_PUSHPULL);		//GCLK
  palSetPadMode(PORT_LED, LED_GCLK, PAL_MODE_OUTPUT_PUSHPULL);
  palClearPad(PORT_LED, LED_GCLK);  
  
  
  palSetPadMode(PORT_LED, PIN_LED_R, PAL_MODE_OUTPUT_PUSHPULL);		//R
  palSetPadMode(PORT_LED, PIN_LED_G, PAL_MODE_OUTPUT_PUSHPULL);		//G
  palSetPadMode(PORT_LED, PIN_LED_B, PAL_MODE_OUTPUT_PUSHPULL);		//B
  palClearPad(PORT_LED, PIN_LED_R | PIN_LED_G | PIN_LED_B);  
    
  wait_ms(1);
  led_set_config();
  wait_ms(1);
  
  //Timer interrupt
  ///gptStart(&GPTD3, &gpt1cfg);
  ///gptStartContinuous(&GPTD3, 10000); // Call Callback every 10000 Ticks (1/2 toggle)
  
  //GClk setup
  led_gclk_set(TRUE);
  
  wait_ms(1);
  dprint("keyboard init done\n");

  int i;
  for(i=0 ; i<16 ; i++)
  {
	  set_led_percent(i, i*5);
  }
  
  //turn off all LEDs
  //clear_leds();
  //update_leds();
}


//=====================================
//Debugging
/*
bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  // If console is enabled, it will print the matrix position and status of each key pressed
#ifdef CONSOLE_ENABLE
    uprintf("KL: kc: %u, col: %u, row: %u, pressed: %u\n", keycode, record->event.key.col, record->event.key.row, record->event.pressed);
#endif 
  return true;
}*/

//=====================================
//Macro keys

//Function will be called whenever key is pressed/released
bool process_record_user(uint16_t keycode, keyrecord_t *record) 
{

//Can light up key when pressed, if within 4x4 matrix
  uint8_t col = record->event.key.col;
  uint8_t row = record->event.key.row;
  uint8_t led = (((3 - row) * 4) + 3 - col) ;
  if((col <= 3) && (row <= 3))
	{
	 	if(record->event.pressed)
			{
			set_led_percent(led, 50);			//turn on pressed key
			}
		else						//turn off unpressed key (but indicators?)
			{
			set_led_percent(led, 0);
			}
	  update_leds();
	}	
	
// If console is enabled, it will print the matrix position and status of each key pressed
#ifdef CONSOLE_ENABLE
    uprintf("KL: kc: %u, col: %u, row: %u, pressed: %u\n", keycode, record->event.key.col, record->event.key.row, record->event.pressed);
#endif 

  switch (keycode) 
  {
    case QMK_2OR3:
	{
      if (record->event.pressed) {
		static bool tapped = true;
		if(tapped)
		{
			//send code '3' for 3D
			SEND_STRING(SS_TAP(X_3));		//taps KC_3
			tapped = false;
		}
		else
		{
			//send code '2' for 2D
			SEND_STRING(SS_TAP(X_2));		//taps KC_2
			tapped = true;
		}
        //SEND_STRING("QMK is the best thing ever!");
      } else {
        // when keycode is released
		// No release needed as tap used
      }
      break;
	}
	  
    case QMK_SHELF:
	{
      if (record->event.pressed) {
        // when keycode QMKURL is pressed
        //SEND_STRING("https://qmk.fm/" SS_TAP(X_ENTER));
      } else {
        // when keycode QMKURL is released
      }
      break;
	}
	  
	  
    case QMK_MACRO:
	{
      if (record->event.pressed) 
	  {
                SEND_STRING(SS_LCTRL("ac")); // selects all and copies
      }
      break;
	}
	
	default:
		break;
  }
  
  return true;
};
