SKIP_GIT = yes			# ignore QMK git

#BOOTMAGIC_ENABLE = yes	# Virtual DIP switch configuration
EXTRAKEY_ENABLE = yes	# Audio control and System control
CONSOLE_ENABLE = yes	# Console for debug
COMMAND_ENABLE = no    # Commands for debug and configuration		*was on
SLEEP_LED_ENABLE = no  # Breathing sleep LED during USB suspend
NKRO_ENABLE = yes	    # USB Nkey Rollover
CUSTOM_MATRIX = yes 	# Custom matrix file
MOUSEKEY_ENABLE = no

DIP_SWITCH_ENABLE = yes

RGB_MATRIX_ENABLE = no
#RGB_MATRIX_ENABLE = custom

BACKLIGHT_ENABLE = no

MBI5043 = TRUE

DEFAULT_FOLDER = handwired/bluepill/bluepill70


#Bootloader, specify PID/VID, alt = flash program location
#DFU_ARGS = -d 1EAF:0003 -a 2

#error sending completion packet seen but still works
#DFU_ARGS = -S "LLM 003" -a 2 -R -D "handwired_bluepill_bluepill70_default.bin"
DFU_ARGS = -d FEED:6464 -a 2 -R -D "handwired_bluepill_bluepill70_default.bin"