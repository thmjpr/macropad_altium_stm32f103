#ifndef IS31FL3731_DRIVER_H
#define IS31FL3731_DRIVER_H

#include <stdint.h>
#include <stdbool.h>

/*
typedef struct is31_led {
    uint8_t driver : 2;
    uint8_t r;
    uint8_t g;
    uint8_t b;
} __attribute__((packed)) is31_led;

extern const is31_led g_is31_leds[DRIVER_LED_TOTAL];

void IS31FL3731_init(uint8_t addr);
void IS31FL3731_write_register(uint8_t addr, uint8_t reg, uint8_t data);
void IS31FL3731_write_pwm_buffer(uint8_t addr, uint8_t *pwm_buffer);

void IS31FL3731_set_color(int index, uint8_t red, uint8_t green, uint8_t blue);
void IS31FL3731_set_color_all(uint8_t red, uint8_t green, uint8_t blue);

void IS31FL3731_set_led_control_register(uint8_t index, bool red, bool green, bool blue);

// This should not be called from an interrupt
// (eg. from a timer interrupt).
// Call this while idle (in between matrix scans).
// If the buffer is dirty, it will update the driver with the buffer.
void IS31FL3731_update_pwm_buffers(uint8_t addr, uint8_t index);
void IS31FL3731_update_led_control_registers(uint8_t addr, uint8_t index);
*/


#define MBI5043_DISABLE					(0<<0)
#define MBI5043_ENABLE					(1<<0)

#define MBI5043_RESERVED1				(0<<1)

#define MBI5043_COLOR_SHIFT_DIS			(0<<2)
#define MBI5043_COLOR_SHIFT_MD1			(1<<2)
#define MBI5043_COLOR_SHIFT_MD2			(2<<2)
#define MBI5043_COLOR_SHIFT_MD3			(3<<2)

#define MBI5043_GCLK_RISING				(0<<3)
#define MBI5043_GCLK_RISINGFALLING		(1<<3)

#define MBI5043_CUR_GAIN_012			(0<<4)
#define MBI5043_CUR_GAIN_50				(20<<4)
#define MBI5043_CUR_GAIN_100			(43<<4)
#define MBI5043_CUR_GAIN_150			(53<<4)
#define MBI5043_CUR_GAIN_200			(63<<4)

#define MBI5043_PRECHARGE_MD1			(0<<0xB)

#define MBI5043_GCLK_SHIFT				(0<<0xE)
//PRECHARGE MODE


#define PERCENT_LED(X)				(uint16_t)(X*655.36)

//extern volatile uint16_t led_states[];

void toggle_dclk(void);
void set_led(uint16_t led, uint16_t brightness);
void set_led_percent(uint16_t led, uint16_t brightness);
void update_leds(void);
void led_set_config(void);
void short_delay_100ns(void);
void led_clear(void);
void led_init(void);
void led_flush(void);

#endif