# project specific files
SRC =	matrix.c \
		mbi5043_LED.c
	

#temp test
ALLOW_WARNINGS = yes
	
#enable rotary encoders
ENCODER_ENABLE = yes

#debugging:
CONSOLE_ENABLE = yes
		
# GENERIC STM32F103C8T6 board - stm32duino bootloader
OPT_DEFS = -DCORTEX_VTOR_INIT=0x2000 -O0
MCU_LDSCRIPT = STM32F103x8_stm32duino_bootloader
BOARD = GENERIC_STM32_F103

# MCU NAME
MCU = STM32F103

# Enter lower-power sleep mode when on the ChibiOS idle thread
OPT_DEFS += -DCORTEX_ENABLE_WFI_IDLE=TRUE
