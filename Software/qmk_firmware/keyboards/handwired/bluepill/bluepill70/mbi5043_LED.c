
#include "hal.h"
#include "ch.h"
#include "board.h"
#include "wait.h"

#include "color.h"
#include "quantum.h"
#include "rgblight_list.h"
#include "rgb_matrix_types.h"

#include "mbi5043_LED.h"

#define NUM_LEDS 16

volatile uint16_t led_states[NUM_LEDS] = {0};		//can store LED brightness levels
bool	buffer_update_required = false;


//-----------------------------------------------
void set_led(uint16_t led, uint16_t brightness)
{
	led_states[led] = brightness;
}

//-----------------------------------------------
void set_led_percent(uint16_t led, uint16_t brightness)
{
	if(brightness > 100)
		brightness = 100;
	
	uint32_t lvl = (brightness * 65536) / 100;
	
	led_states[led] = (uint16_t)lvl;
}


void led_clear(void)
{
	for(uint32_t i=0 ; i<NUM_LEDS ; i++)
	{
		led_states[i] = 0;
	}
	
	uprintf("clear led ");
}

void toggle_dclk(void)
{
  short_delay_100ns();
  palSetPad(PORT_LED, LED_DCLK);
  short_delay_100ns();
  palClearPad(PORT_LED, LED_DCLK);
  short_delay_100ns();
}

void short_delay_100ns(void)
{
  for(uint32_t i=0; i<600; i++)
  {}	
}

//-----------------------------------------------
//

void led_set_config(void)
{
	uint16_t config = 	MBI5043_DISABLE | \
						MBI5043_COLOR_SHIFT_DIS | 	\
						MBI5043_GCLK_RISINGFALLING | \
						MBI5043_CUR_GAIN_100;				//Brightness level


 //Enable write configuration
  for (uint16_t i = 0; i < 16; i++)
  {
	  palSetPad(PORT_LED, LED_SDI);
	  toggle_dclk();  
	  
	if(i == 0)
	{
		palSetPad(PORT_LED, LED_LE);
	}		
  }
  palClearPad(PORT_LED, LED_LE);		//Clear latch
  
  toggle_dclk();toggle_dclk();
  
   //Enable write configuration
  for (uint16_t i = 0; i < 16; i++)
  {
	  palClearPad(PORT_LED, LED_SDI);
	  toggle_dclk();  
	  
	if(i == 1)
	{
		palSetPad(PORT_LED, LED_LE);
	}		
  }
  palClearPad(PORT_LED, LED_LE);		//Clear latch

 toggle_dclk();toggle_dclk();

 //Writing configuration
  for (uint16_t i = 0; i < 16; i++)
  {
	  if(config & ((1<<15)>>i))		//data 1
	  {
		  palSetPad(PORT_LED, LED_SDI);
	  }
	  else
	  {
		  palClearPad(PORT_LED, LED_SDI);
	  }
	  toggle_dclk();
	  
	if(i == 4)
	{
		palSetPad(PORT_LED, LED_LE);
	}		
  }
  palClearPad(PORT_LED, LED_LE);		//Clear latch
  

  toggle_dclk();toggle_dclk();
}

//-----------------------------------------------------
// Take current data and update LED levels
void update_leds(void)
{
	//16-bit value to send to LEDs
  for(uint16_t k = 0; k < 16; k++)	//send data for 16 LEDs total (LED15 is first -> LED0 last)
  {
  //Data latch setup (send 16-bit data to buffer for one LED)
  for (uint16_t i = 0; i < 16; i++)
  {
	  if(led_states[k] & ((1<<15)>>i))		//data 1
	  {
		  palSetPad(PORT_LED, LED_SDI);
	  }
	  else
	  {
		  palClearPad(PORT_LED, LED_SDI);
	  }
	  toggle_dclk();  
	  
	if(i == 14)
	{
		palSetPad(PORT_LED, LED_LE);
	}		
  }
  palClearPad(PORT_LED, LED_LE);		//Clear latch
  }
  
 
  //Global latch (output buffer data on LEDs)
  for (uint16_t i = 0; i < 16; i++)
  {
	  if(led_states[0] & ((1<<15)>>i))		//data 1
	  {
		  palSetPad(PORT_LED, LED_SDI);
	  }
	  else
	  {
		  palClearPad(PORT_LED, LED_SDI);
	  }	  
	  toggle_dclk();  
	  
	if(i == 12)		//latch after 
	{
		palSetPad(PORT_LED, LED_LE);
	}		
  }
  
  palClearPad(PORT_LED, LED_LE);		//Clear latch	
}


//===================== Matrix stuff =================
//

void MBI5043_set_color(int index, uint8_t red, uint8_t green, uint8_t blue) {
    if (index >= 0 && index < DRIVER_LED_TOTAL) 
	{   
		set_led(index, (uint16_t)(red | green | blue)<<8);			//Just set the one color
        buffer_update_required = true;		
    }
	uprintf("colorl led ");
}

void MBI5043_set_color_all(uint8_t red, uint8_t green, uint8_t blue) {
    for (int i = 0; i < DRIVER_LED_TOTAL; i++) 
	{
		set_led(i, (uint16_t)(red | green | blue)<<8);			//Just set the one color
		//set_led(i, (uint16_t)red<<8);		//take 8-bit value shift to 16-bit
    }
	buffer_update_required = true;
	uprintf("color led ");
}

void led_init(void) 
{
    //IS31FL3731_init(DRIVER_ADDR_1);

	//set register 
	
	//Turn off all LEDs
	led_clear();
	update_leds();
	
	//dprintf("init ");
}

void led_flush(void) 
{
	//if(buffer_update_required)
    update_leds();
	uprintf("flush led ");
}

#ifdef RGB_MATRIX_ENABLE
const rgb_matrix_driver_t rgb_matrix_driver = 
{
    .init          = led_init,
    .flush         = led_flush,
    .set_color     = MBI5043_set_color,
    .set_color_all = MBI5043_set_color_all,
};
#endif
