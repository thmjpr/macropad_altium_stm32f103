#pragma once

/* USB Device descriptor parameter */
#define VENDOR_ID       0xFEED
#define PRODUCT_ID      0x6464
#define DEVICE_VER      0x0001
/* in python2: list(u"whatever".encode('utf-16-le')) */
/*   at most 32 characters or the ugly hack in usb_main.c works */


#define MANUFACTURER "Thomas"
#define USBSTR_MANUFACTURER    'T', '\x00', 'M', '\x00', 'K', '\x00', ' ', '\x00'
#define PRODUCT "Macro 4x4 Keyboard"
#define USBSTR_PRODUCT         'T', '\x00', 'h', '\x00', 'm', '\x00'
#define DESCRIPTION "Keyboard firmware"

/* key matrix size */
#define MATRIX_ROWS 4
#define MATRIX_COLS 6
#define DIODE_DIRECTION COL2ROW

// Iso fix for Space Cadet, comment for ANSI layouts
//#define LSPO_KEY KC_8
//#define RSPC_KEY KC_9

//#define DEBUG_MATRIX_SCAN_RATE  //how often matrix is scanned frequency