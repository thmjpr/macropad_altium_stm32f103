/*
Copyright 2012 Jun Wako <wakojun@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ch.h"
#include "hal.h"

/*
 * scan matrix
 */
#include "print.h"
#include "debug.h"
#include "util.h"
#include "matrix.h"
#include "wait.h"

#ifndef DEBOUNCE
#   define DEBOUNCE 5
#endif
static uint8_t debouncing = DEBOUNCE;

/* matrix state(1:on, 0:off) */
static matrix_row_t matrix[MATRIX_ROWS];
static matrix_row_t matrix_debouncing[MATRIX_ROWS];

static matrix_row_t read_cols(void);
static void init_cols(void);
static void unselect_rows(void);
static void select_row(uint8_t row);

inline uint8_t matrix_rows(void){
  return MATRIX_ROWS;
}

inline uint8_t matrix_cols(void){
  return MATRIX_COLS;
}

//==========================================
__attribute__ ((weak))
void matrix_init_kb(void) {
    matrix_init_user();
}

__attribute__ ((weak))
void matrix_scan_kb(void) {
    matrix_scan_user();
}

__attribute__ ((weak))
void matrix_init_user(void) {
}

__attribute__ ((weak))
void matrix_scan_user(void) {
}

void matrix_init(void)
{
  // initialize row and col
  unselect_rows();
  init_cols();
  // initialize matrix state: all keys off
  for (uint8_t i=0; i < MATRIX_ROWS; i++) {
    matrix[i] = 0;
    matrix_debouncing[i] = 0;
  }
  //debug
  debug_matrix = true;

  matrix_init_quantum();
}

uint8_t matrix_scan(void){
  for (uint8_t i = 0; i < MATRIX_ROWS; i++) {
    select_row(i);
    wait_us(100);//wait_us(30);  // without this wait read unstable value.
    matrix_row_t cols = read_cols();
	
    if (matrix_debouncing[i] != cols) {
      matrix_debouncing[i] = cols;
      if (debouncing) {
		  
        debug("bounce!: "); debug_hex(debouncing); debug("\n");
      }
	  
    debouncing = DEBOUNCE;
    }
    unselect_rows();
  }

  if (debouncing) {
    if (--debouncing) {
	  wait_ms(1);							//It is calling chThdSleepMilliseconds(1); OK
    } else {
      for (uint8_t i = 0; i < MATRIX_ROWS; i++) {
        matrix[i] = matrix_debouncing[i];
      }
    }
  }
  matrix_scan_quantum();
  return 1;
}

inline bool matrix_is_on(uint8_t row, uint8_t col){
  return (matrix[row] & ((matrix_row_t)1<<col));
}

inline matrix_row_t matrix_get_row(uint8_t row){
  return matrix[row];
}


void matrix_print(void){
	/*
  dprint("\nr/c 01234567       btn:\n");
  for (uint8_t row = 0; row < MATRIX_ROWS; row++) 
  {
    phex(row); print(": ");
	phex(matrix_get_row(row));
	phex(read_cols());
    dprint("\n");
  }
  
//  if(matrix_has_ghost_in_row(row))
//	  print("ghost");
  */
}


/* Column pin configuration
 */

static void  init_cols(void){
  palSetPadMode(COL_PORT, COL_0, PAL_MODE_INPUT_PULLUP);
  palSetPadMode(COL_PORT, COL_1, PAL_MODE_INPUT_PULLUP);
  palSetPadMode(COL_PORT, COL_2, PAL_MODE_INPUT_PULLUP);
  palSetPadMode(COL_PORT, COL_3, PAL_MODE_INPUT_PULLUP);
  palSetPadMode(COL_PORT, COL_4, PAL_MODE_INPUT_PULLUP);
  palSetPadMode(COL_PORT, COL_5, PAL_MODE_INPUT_PULLUP);
	
  palSetPadMode(ROW_PORT, ROW_0, PAL_MODE_INPUT);
  palSetPadMode(ROW_PORT, ROW_1, PAL_MODE_INPUT);
  palSetPadMode(ROW_PORT, ROW_2, PAL_MODE_INPUT);
  palSetPadMode(ROW_PORT, ROW_3, PAL_MODE_INPUT);
 
}


/* Returns status of switches(1:on, 0:off) */
static matrix_row_t read_cols(void){
  return (\
	   (palReadPad(COL_PORT, COL_0)==PAL_HIGH) ? 0 : (1<<0))
    | ((palReadPad(COL_PORT, COL_1)==PAL_HIGH) ? 0 : (1<<1))
    | ((palReadPad(COL_PORT, COL_2)==PAL_HIGH) ? 0 : (1<<2))
    | ((palReadPad(COL_PORT, COL_3)==PAL_HIGH) ? 0 : (1<<3))
    | ((palReadPad(COL_PORT, COL_4)==PAL_HIGH) ? 0 : (1<<4))
    | ((palReadPad(COL_PORT, COL_5)==PAL_HIGH) ? 0 : (1<<5));
	/*
    | ((palReadPad(GPIOB, 14)==PAL_HIGH) ? 0 : (1<<6))
    | ((palReadPad(GPIOB, 13)==PAL_HIGH) ? 0 : (1<<7))
    | ((palReadPad(GPIOB, 12)==PAL_HIGH) ? 0 : (1<<8))
    | ((palReadPad(GPIOB, 11)==PAL_HIGH) ? 0 : (1<<9))
    | ((palReadPad(GPIOB, 10)==PAL_HIGH) ? 0 : (1<<10))
    | ((palReadPad(GPIOB, 1)==PAL_HIGH) ? 0 : (1<<11))
    | ((palReadPad(GPIOB, 0)==PAL_HIGH) ? 0 : (1<<12))
    | ((palReadPad(GPIOA, 7)==PAL_HIGH) ? 0 : (1<<13))
    | ((palReadPad(GPIOA, 6)==PAL_HIGH) ? 0 : (1<<14));*/
	
	//could have "virtual" row/col for enc but kind of stupid..
	//palReadPad(ENC_PORT, ENC0_SW)==PAL_HIGH
}

/* Row pin configuration
 */
static void unselect_rows(void){
  palSetPadMode(ROW_PORT, ROW_0, PAL_MODE_INPUT);
  palSetPadMode(ROW_PORT, ROW_1, PAL_MODE_INPUT);
  palSetPadMode(ROW_PORT, ROW_2, PAL_MODE_INPUT);
  palSetPadMode(ROW_PORT, ROW_3, PAL_MODE_INPUT);
}


static void select_row(uint8_t row){
  (void)row;
  switch (row) {
    case 0:
      palSetPadMode(ROW_PORT, ROW_0, PAL_MODE_OUTPUT_PUSHPULL);
      palClearPad(ROW_PORT, ROW_0);
      break;
    case 1:
      palSetPadMode(ROW_PORT, ROW_1, PAL_MODE_OUTPUT_PUSHPULL);
      palClearPad(ROW_PORT, ROW_1);
      break;
    case 2:
      palSetPadMode(ROW_PORT, ROW_2, PAL_MODE_OUTPUT_PUSHPULL);
      palClearPad(ROW_PORT, ROW_2);
      break;
    case 3:
      palSetPadMode(ROW_PORT, ROW_3, PAL_MODE_OUTPUT_PUSHPULL);
      palClearPad(ROW_PORT, ROW_3);
      break;
	default:
		dprint("switch error select row");
		break;
  }
}
