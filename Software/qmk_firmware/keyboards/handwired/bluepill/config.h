/*
Copyright 2015 Jun Wako <wakojun@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
/* define if matrix has ghost */
//#define MATRIX_HAS_GHOST

/* Set 0 if debouncing isn't needed */
#define DEBOUNCE    7

//Testing for media key issue
#define TAP_CODE_DELAY 10


//Define additional inputs (encoders)
//Third encoder maybe not needed..
#define ENCODERS_PAD_A		{ B0, B3, B6}
#define ENCODERS_PAD_B		{ B1, B4, B7}
#define ENCODER_RESOLUTION	4
//Would use defines but they are weird...
#define DIP_SWITCH_PINS { B2, B5, C14}



//LED Stuff
#define DRIVER_COUNT 1
#define DRIVER_1_LED_TOTAL 16
#define DRIVER_2_LED_TOTAL 0
#define DRIVER_LED_TOTAL (DRIVER_1_LED_TOTAL + DRIVER_2_LED_TOTAL)

#define RGB_MATRIX_CENTER {1, 1};

#define RGB_MATRIX_KEYPRESSES			//light on keypress
#define RGB_DISABLE_AFTER_TIMEOUT 500 	// number of ticks to wait until disabling effects
#define RGB_DISABLE_WHEN_USB_SUSPENDED true		// turn off effects when suspended
//#define RGB_MATRIX_LED_PROCESS_LIMIT (DRIVER_LED_TOTAL + 4) / 5 // limits the number of LEDs to process in an animation per task run (increases keyboard responsiveness)
#define RGB_MATRIX_LED_FLUSH_LIMIT 16 // limits in milliseconds how frequently an animation will update the LEDs. 16 (16ms) is equivalent to limiting to 60fps (increases keyboard responsiveness)
//#define RGB_MATRIX_MAXIMUM_BRIGHTNESS 200 // limits maximum brightness of LEDs to 200 out of 255. If not defined maximum brightness is set to 255
#define RGB_MATRIX_STARTUP_MODE RGB_MATRIX_CYCLE_LEFT_RIGHT // Sets the default mode, if none has been set



#define RGBLIGHT_EFFECT_BREATHE_MAX 200

//Styles to disable??
#define DISABLE_RGB_MATRIX_SOLID_REACTIVE
#define DISABLE_RGB_MATRIX_RAINBOW_BEACON
#define DISABLE_RGB_MATRIX_RAINBOW_PINWHEELS
#define DISABLE_RGB_MATRIX_RAINDROPS
#define DISABLE_RGB_MATRIX_JELLYBEAN_RAINDROPS

//RGB_MATRIX_SOLID_REACTIVE_SIMPLE


/* Mechanical locking support. Use KC_LCAP, KC_LNUM or KC_LSCR instead in keymap */
//#define LOCKING_SUPPORT_ENABLE
/* Locking resynchronize hack */
//#define LOCKING_RESYNC_ENABLE

/* Backlighting include */
//#define BACKLIGHT_PIN 19
#define BACKLIGHT_LEVELS 5
//#define BACKLIGHT_BREATHING	//need to include file led.c
//#define BREATHING_PERIOD 6


/*
 * Feature disable options
 *  These options are also useful to firmware size reduction.
 */

/* disable debug print */
//#define NO_DEBUG

/* disable print */
//#define NO_PRINT

/* disable action features */
//#define NO_ACTION_LAYER
#define NO_ACTION_TAPPING
#define NO_ACTION_ONESHOT
#define NO_ACTION_MACRO
#define NO_ACTION_FUNCTION